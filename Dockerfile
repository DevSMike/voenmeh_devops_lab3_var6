FROM ubuntu:latest

COPY ./build/bin/*.deb /tmp/

RUN apt-get update && apt-get install -y /tmp/*.deb && rm -rf /var/lib/apt/lists/* && rm /tmp/*.deb

CMD ["bash"]

