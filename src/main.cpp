#include <iostream>
#include "reverse.h"

using namespace std;

int main(int argc, char* argv[])
{
    string s;

    if (argc < 2) {
        cout << "Enter a string: ";
        getline(cin, s);
    } else {
        s = argv[1];
    }

    cout <<  reverse_words(s) << endl;
    return 0;
}